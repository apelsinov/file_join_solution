import json
import csv
import requests

RESULT_FILE = "result.json"
USERS_FILE = "users.json"
BOOKS_FILE = "books.csv"
STORAGE = "https://raw.githubusercontent.com/konflic/front_example/master/data"


def download_test_data():
    users_file_path = f"{STORAGE}/{USERS_FILE}"
    books_file_path = f"{STORAGE}/{BOOKS_FILE}"

    with open(USERS_FILE, "w") as f:
        f.write(requests.get(users_file_path).text)

    with open(BOOKS_FILE, "w") as f:
        f.write(requests.get(books_file_path).text)


download_test_data()

result = []

with open(BOOKS_FILE) as f_csv:
    books = csv.DictReader(f_csv)

    with open(USERS_FILE) as u_json:
        users = json.load(u_json)
        users_amount = len(users)

        for user in users:
            result.append(
                {
                    "name": user["name"], "gender": user["gender"],
                    "address": user["address"],
                    "age": user["age"],
                    "books": []
                }
            )

        index = 0
        book = next(books)

        while books:
            user = result[index % users_amount]

            if (user["age"] > 25) and (book["Genre"] == "comic"):
                index += 1
                continue

            if (user["age"] < 30) and (book["Genre"] == "history"):
                index += 1
                continue

            user["books"].append(
                {
                    "title": book["Title"],
                    "author": book["Author"],
                    "pages": int(book["Pages"]),
                    "genre": book["Genre"]
                }
            )

            try:
                book = next(books)
            except StopIteration:
                break

            index += 1

with open(RESULT_FILE, "w") as f_result:
    f_result.write(json.dumps(result))
